import axios from 'axios';

const categories = {
    lista: (params = {}) => {
        return axios.get('/api/categories');
    },
};

export const lista = categories.lista;

export default categories;
