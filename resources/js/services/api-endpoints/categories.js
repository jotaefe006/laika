import baseUrl from './base-url.js';

const categoriesEndpoints = {
    categories: `${baseUrl}/categories/`
};

export const categories = categoriesEndpoints.categories;
export default categoriesEndpoints;
