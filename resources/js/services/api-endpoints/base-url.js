/**
 * javascript comment
 * @Author: Jhon Frey Diaz
 * @Date: 2022-02-05 16:06:17
 * @Desc: Coleccion de endpoints API del aplicativo.
 */

 const apiURL = process.env.MIX_API_URL.trim().replace(/\/$/, '');

 const baseUrl = `${apiURL}`;

 export default baseUrl;
