import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

window.Vue = require('vue').default;

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
import "./bootstrap"
import App from  './views/App.vue';
import axios from 'axios';

// Axios global config
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.withCredentials = true;

window.onload = function() {
    const app = new Vue({
        el: '#app',
        render: h => h(App)
    });
}

export default app;

